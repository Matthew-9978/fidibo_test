import 'package:flutter/material.dart';

class ShapeOptionWidget extends StatelessWidget {
  final VoidCallback onAdd;
  final String icon;

  const ShapeOptionWidget({
    Key? key,
    required this.onAdd,
    required this.icon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: onAdd,
      icon: Image.asset(
        icon,
        height: 30,
        width: 30,
        fit: BoxFit.fill,
      ),
    );
  }
}
