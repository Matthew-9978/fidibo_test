import 'package:fidibo_test/models/shape.dart';
import 'package:fidibo_test/utils/shape_type_extension.dart';
import 'package:flutter/material.dart';

class ShapeWidget extends StatelessWidget {
  final Shape shape;

  const ShapeWidget({
    Key? key,
    required this.shape,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: shape.alignment,
      child: SizedBox(
        height: shape.width,
        width: shape.width,
        child: Image.asset(
          shape.shapeType.getIcon,
          height: shape.width,
          width: shape.width,
          color: shape.shapeType.colorByShapeType,
          fit: BoxFit.fill,
        ),
      ),
    );
  }


}
