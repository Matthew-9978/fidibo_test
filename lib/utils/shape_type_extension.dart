import 'package:fidibo_test/models/shape.dart';
import 'package:flutter/material.dart';

extension ShapeTypeExtension on ShapeType{
  Color get colorByShapeType{
    switch(this){
      case ShapeType.rock:
        return Colors.grey;
      case ShapeType.paper:
        return Colors.blue;
      case ShapeType.scissor:
        return Colors.red;
    }
  }

  String get getIcon {
    switch (this) {
      case ShapeType.rock:
        return "assets/png/rock.png";
      case ShapeType.paper:
        return "assets/png/paper.png";
      case ShapeType.scissor:
        return "assets/png/scissors.png";
    }
  }
}